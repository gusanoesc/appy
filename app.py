from flask import Flask, request

application = Flask(__name__)


@application.route('/', methods=['GET'])
def index():
    if request.method == 'GET':
        return 'hola desde python'


if __name__ == "__main__":
    from os import environ
    application.run(host='0.0.0.0', port=8080, debug=True)
